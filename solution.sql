-- No .1
SELECT customerName FROM customers WHERE country = "Philippines";
-- No .2
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";
-- No .3
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";
-- No .4
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";
-- No .5
SELECT customerName FROM customers WHERE state IS NULL; 
-- No.6
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";
-- No .7
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;
-- No .8
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";
-- No .9
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";
-- No .10
SELECT DISTINCT country FROM customers;
-- No .11
SELECT DISTINCT status FROM orders;
-- No .12
SELECT customerName, country FROM customers WHERE country IN("USA","France", "Canada");
-- No .13
SELECT employees.firstName, employees.lastName, offices.city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE offices.city = "Tokyo";
-- No .14
SELECT customers.customerName FROM customers
	JOIN employees on customers.salesRepEmployeeNUmber = employees.employeeNumber
	WHERE employees.lastName = "Thompson" AND employees.firstName = "Leslie";
-- No .15
SELECT products.productName, customers.customerName FROM products
	INNER JOIN orderdetails ON products.productCode = orderdetails.productCode
	INNER JOIN orders ON orderdetails.orderNumber = orders.orderNumber
	INNER JOIN customers ON orders.customerNumber = customers.customerNumber
	WHERE customerName = "Baane Mini Imports";
-- No .16
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country from customers
	LEFT JOIN offices on customers.city = offices.city
	INNER JOIN employees on offices.officeCode = employees.officeCode;
-- No .17
SELECT productName, quantityInStock FROM products
	WHERE productLine = "Planes" AND quantityInStock > 1000;
-- No .18
SELECT customerName FROM customers WHERE phone LIKE "+81%"